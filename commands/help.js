/*
- The HELP command is used to display every command's name and description
to the user, so that he may see what commands are available
- If a command name is given with the help command, its extended help is shown
*/

exports.run = (client, message, args) => {
    // If no specific command is called, show all filtered commands
    if (args.length === 0) {
        // Here we have to get the command names only, and we use that array to get the longest name
        // This make the help commands "aligned" in the output
        const commandNames = Object.keys(client.commands);
        const longest = commandNames.reduce((long, str) => Math.max(long, str.length), 0);

        let currentCategory = '';
        let output = `= Command List =\n\n[Use ${client.config.prefix}help <command> for details]\n`;
        const sorted = Object.values(client.commands).sort((p, c) => {
            if (p.help.category > c.help.category) {
                return 1;
            }
            else if (p.help.name > c.help.name && p.help.category === c.help.category) {
                return 1;
            }
            return -1;
        });
        sorted.forEach((c) => {
            const cat = c.help.category;
            if (currentCategory !== cat) {
                output += `\u200b\n== ${cat} ==\n`;
                currentCategory = cat;
            }
            output += `${client.config.prefix}${c.help.name}${' '.repeat(longest - c.help.name.length)} :: ${c.help.description}\n`;
        });

        message.channel.send(output, { code: 'asciidoc', split: { char: '\u200b' } })
            .catch(console.error);
    }
    else {
    // Show individual command's help
        let commandAsked = args[0];
        if (Object.keys(client.commands).includes(commandAsked)) {
            commandAsked = client.commands[commandAsked];
            message.channel.send(`= ${commandAsked.help.name} = \n${commandAsked.help.description}\nusage :: ${client.config.prefix}${commandAsked.help.usage}`, { code: 'asciidoc' })
                .catch(console.error);
        }
    }
};


exports.help = {
    name: 'help',
    category: 'System',
    description: 'Displays all the available commands.',
    usage: 'help [<command>]',
};

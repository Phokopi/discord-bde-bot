module.exports.prefix = '.';

module.exports.commandsDirectory = `${__dirname}/commands`;

module.exports.activityMessage = `${this.prefix}help`;

// the channel the bot will read and delete messages
module.exports.authorizedChans = ['451696544085704704'];

module.exports.removeMessageRole = true;

module.exports.idChanLogQuit = '451685969716838402';

module.exports.idChanOnly0A = '592779322226507776';

module.exports.idChanSpam0A = '552105790379524097';

module.exports.idChanSerieux0A = '592779142961823765';

module.exports.idChanLogement0A = '595585655275257856';

module.exports.idRole0A = '449295406459781132';

module.exports.idChanPresentations0A = '552105545218260992';

/*
- In aliases array, only write in lower case (so that we don't need to apply the toLowerCase function to each alias array)
- id should be a string, because it would be too big as an int
*/
const roles = {
    Alcoolique: {
        id: '612927830757474314',
        alias: [],
    },
    Alumni: {
    	id: '613114508155682818',
    	alias: ['a3ie'],
    },
    Aperal: {
    	id: '613114868131823700',
    	alias: ['apéral'],
    },
    Arise: {
        id: '441363462552027146',
        alias: [],
    },
    Badminton: {
        id: '625996630268575754',
        alias: ['bad'],
    },
    Bakaclub: {
        id: '441356700168224784',
        alias: ['baka', 'bakateux'],
    },
    BDSF: {
        id: '458265466864140309',
        alias: ['bd-sf'],
    },
    CCCM: {
        id: '441362953774825483',
        alias: ['fanfare'],
    },
    CuIsInE: {
        id: '441367326823153674',
        alias: [],
    },
    CID: {
        id: '441522926609498114',
        alias: [],
    },
    CiSAiLLe: {
    	id: '613114940919906306',
    	alias: [],
    },
    CoinchIIE: {
    	id: '613115004161359899',
    	alias: [],
    },
    ComDIIE: {
    	id: '613115085619200000',
    	alias: [],
    },
    CraftIIE: {
    	id: '613115130733002762',
    	alias: [],
    },
    DansIIE: {
        id: '441357296916889600',
        alias: [],
    },
    Diese: {
        id: '441368522623287296',
        alias: ['#'],
    },
    EchequIIE: {
    	id: '613115175368785961',
    	alias: [],
    },
    EcologIIE: {
        id: '441893117277372416',
        alias: ['ecologiie'],
    },
    FiLiGRANe: {
        id: '441523099851292672',
        alias: ['magic'],
    },
    FinancIIE: {
    	id: '613116053425356847',
    	alias: [],
    },
    ForumIIE: {
        id: '441561935620734988',
        alias: ['forum'],
    },
    Gala: {
        id: '444971991582244864',
        alias: [],
    },
    GuIIldE: {
        id: '441538039756488725',
        alias: ['jdr'],
    },
    Houdiniie: {
        id: '441530506106896387',
        alias: [],
    },
    HumanIIE: {
    	id: '613115223800414374',
    	alias: [],
    },
    ITV: {
        id: '441531527193427988',
        alias: ['i-TV'],
    },
    IIEplusplus: {
    	id: '613115371276206144',
    	alias: ['iie++'],
    },
    IImondE: {
        id: '458020017200693248',
        alias: [],
    },
    IInnovE: {
    	id: '613115407020326913',
    	alias: [],
    },
    InclusIvE: {
        id: '651833780528742430',
        alias: [],
    },
    InsanitIIE: {
        id: '451994637687848971',
        alias: [],
    },
    LanPartIIE: {
        id: '442764349983948801',
        alias: ['lp'],
    },
    LeBarC: {
        id: '441360682789634048',
        alias: ['bar', 'barman'],
    },
    Liien: {
        id: '441352757123874816',
        alias: [],
    },
    LudIIE: {
    	id: '613115446190931984',
    	alias: [],
    },
    LumIIEre: {
        id: '458265769600352266',
        alias: [],
    },
    MartIIalE: {
    	id: '613115483474100224',
    	alias: [],
    },
    MisterIIE: {
    	id: '613115514385989653',
    	alias: [],
    },
    Muzzik: {
        id: '441554528324550666',
        alias: [],
    },
    Nightiies: {
        id: '448900305480974336',
        alias: ['dj'],
    },
    Oenologiie: {
        id: '472046020784553995',
        alias: [],
    },
    ORiGiNe: {
        id: '613115557767807040',
        alias: [],
    },
    PomPom: {
        id: '451706037184233492',
        alias: [],
    },
    RadIoactIvE: {
        id: '441557454736719873',
        alias: ['radio'],
    },
    RealItIE: {
        id: '441371485869703198',
        alias: [],
    },
    ScIIEnce: {
    	id: '613115617570193409',
    	alias: [],
    },
    SecuritIIE: {
    	id: '613115649366949888',
    	alias: [],
    },
    Siieste: {
        id: '441370359191568384',
        alias: [],
    },
    SourIIrE: {
    	id: '613115680107003925',
    	alias: [],
    },
    VocalIIsE: {
        id: '451705865649782794',
        alias: [],
    },
    Volley: {
        id: '625994616654528513',
        alias: [],
    },
};
module.exports.roles = roles;
